from itemloaders.processors import TakeFirst, MapCompose
from w3lib.html import remove_tags
from scrapy import Field, Item
from scrapy.loader import ItemLoader

class BookItem(Item):
    # define the fields for your item here like:
    title = Field()
    price = Field()
    image_url = Field()
    detail_page_url = Field()

class BookItemLoader(ItemLoader):
    default_item_class = BookItem
    default_output_processor = TakeFirst()
    title_in = MapCompose(remove_tags)
    price_in = MapCompose(remove_tags)

class QuoteItem(Item):
    quote = Field()
    author = Field()
    tags = Field()
