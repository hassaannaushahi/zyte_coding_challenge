# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import logging


class ZyteCodingChallengePipeline:
    def process_item(self, item, spider):
        if spider.name == 'books.toscrape.com':
            try:
                if 'media/cache/' not in item.get('image_url'):
                    logging.warning('Book image url is invalid')
            except:
                if not item.get('image_url'):
                    logging.warning('Book image url is missing')

            if not item.get('title'):
                logging.warning('Book title is missing')

            if not item.get('price'):
                logging.warning('Book price is missing')

            if not item.get('detail_page_url'):
                logging.warning('Book detail page url is missing')

        return item

