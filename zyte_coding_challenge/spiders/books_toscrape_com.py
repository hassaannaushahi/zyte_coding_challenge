import scrapy
from zyte_coding_challenge.items import BookItemLoader


class BooksToscrapeComSpider(scrapy.Spider):
    name = 'books.toscrape.com'
    start_urls = ['http://books.toscrape.com/']

    def parse(self, response):
        categories = response.css('.nav-list ul li a::attr(href)').getall()
        for category in categories:
            yield response.follow(category, self.parse_books)

    def parse_books(self, response):
        for book in response.css('.product_pod'):
            loader = BookItemLoader(selector = book)
            loader.add_css('title', 'h3 a::attr(title)')
            loader.add_css('price', '.price_color::text')
            image_url = book.css('.image_container a img::attr(src)').get()
            loader.add_value('image_url', response.urljoin(image_url))
            detail_page = book.css('h3 a::attr(href)').get()
            loader.add_value('detail_page_url', response.urljoin(detail_page))

            yield loader.load_item()

        next_page = response.css('.next a::attr(href)').get()
        if next_page:
            yield response.follow(next_page, self.parse_books)