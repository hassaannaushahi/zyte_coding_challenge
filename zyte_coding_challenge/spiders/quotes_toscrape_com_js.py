import scrapy
from scrapy_splash import SplashRequest
from zyte_coding_challenge.items import QuoteItem

class QuotesToscrapeComJsSpider(scrapy.Spider):
    name = 'quotes_toscrape_com_js'

    custom_settings = {
        "SPIDER_MIDDLEWARES": {
        'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
        },
        "SPLASH_URL": 'http://localhost:8050/',
        "DOWNLOADER_MIDDLEWARES": {
        'scrapy_splash.SplashCookiesMiddleware': 723,
        'scrapy_splash.SplashMiddleware': 725,
        'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
        },
        "DUPEFILTER_CLASS": 'scrapy_splash.SplashAwareDupeFilter',
        "HTTPCACHE_STORAGE": 'scrapy_splash.SplashAwareFSCacheStorage'
        }

    def start_requests(self):
        yield SplashRequest('http://quotes.toscrape.com/js/', self.parse_quotes)

    def parse_quotes(self, response):
        item = QuoteItem()
        quote_containers = response.css('.quote')
        for quote_container in quote_containers:
            item['quote'] = quote_container.css('.text::text').get()
            item['author'] = quote_container.css('.author::text').get()
            item['tags'] = quote_container.css('.tags > a.tag::text').getall()

            yield item
        next_page = response.css('.next a::attr(href)').get()
        if next_page:
            yield SplashRequest(response.urljoin(next_page), self.parse_quotes)